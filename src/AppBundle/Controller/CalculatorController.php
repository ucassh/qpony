<?php

namespace AppBundle\Controller;

use AppBundle\Service\Calculation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/api", name="api_root")
 */
class CalculatorController extends Controller
{
    /**
     * @Route("/calculate", name="calc_endpoint")
     */
    public function calculateAction(Request $request, Calculation $calculation)
    {
        $input = $request->get('input');

        try {
            $data = $calculation->getResult($input);
        } catch (\InvalidArgumentException $e) {
            $data = 'Błąd danych wejściowych';
        }
        return $this->json(array('data' => $data));
    }
}
