<?php

namespace AppBundle\Command;

use AppBundle\Service\Calculation;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CalculateCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('q:calc')
            ->setDescription('Calculate stuff')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (0 !== ftell(STDIN)) {
            throw new \RuntimeException('Numbers should be provided via stdIn');
        }

        $data = '';
        while (!feof(STDIN)) {
            $data .= fread(STDIN, 1024);
        }

        try {
            /** @var Calculation $calc */
            $service = new Calculation();
            $result = $service->getResult($data);

            foreach ($result as $item) {
                $output->writeln(implode(array_keys($item)) . ' => ' . implode(array_values($item)));
            }
        } catch (\InvalidArgumentException $e) {
            $output->writeln('Błąd danych wejściowych');
        }
    }
}