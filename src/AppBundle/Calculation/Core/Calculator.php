<?php

namespace AppBundle\Calculation\Core;


class Calculator
{
    private $aSeq;

    public function __construct()
    {
        $this->aSeq[0] = 0;
        $this->aSeq[1] = 1;

        for ($i = 2; $i < 100000; $i += 2) {
            $this->aSeq[$i] = $this->aSeq[$i / 2];
            $this->aSeq[$i + 1] = $this->aSeq[$i / 2] + $this->aSeq[$i / 2 + 1];
        }
    }

    public function getResultFor($value)
    {
        $max = 0;
        for ($i = 1; $i <= $value; $i++) {
            if ($this->aSeq[$i] > $max) {
                $max = $this->aSeq[$i];
            }
        }
        return $max;
    }
}
