<?php

namespace AppBundle\Calculation\ValueObject;

class IntegerValueObject implements ValueObject
{
    /**
     * @var int
     */
    private $value;

    /**
     * IntegerValueObject constructor.
     * @param $value
     * @throws \InvalidArgumentException
     */
    public function __construct($value)
    {
        $this->validate($value);
        $this->value = (int)$value;
    }

    /**
     * @param mixed $value
     * @throws \InvalidArgumentException
     */
    protected function validate($value)
    {
        if (empty($value)) {
            throw new \InvalidArgumentException("Value must be provided");
        }
        if (!preg_match('/^\d+$/', $value)) {
            throw new \InvalidArgumentException("Expected integer, \"{$value}\" given");
        }
    }

    /**
     * @return int
     */
    public function value()
    {
        return $this->value;
    }
}
