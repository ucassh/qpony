<?php

namespace AppBundle\Calculation\ValueObject;

interface ValueObject
{
    public function value();
}
