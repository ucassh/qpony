<?php

namespace AppBundle\Calculation\Mapper;

use AppBundle\Calculation\ValueObject\IntegerValueObject;

class InputDataToValueObjects
{
    protected $input;

    public function __construct($input)
    {
        $this->input = $input;
    }

    /**
     * @throws \InvalidArgumentException
     * @return \ArrayObject
     */
    public function getValueObjects()
    {
        return new \ArrayObject(
            array_map(
                function ($item) {
                    return new IntegerValueObject(trim($item));
                },
                explode(PHP_EOL, $this->input)
            )
        );
    }

    /**
     * @return mixed
     */
    public function getRawInput()
    {
        return $this->input;
    }
}