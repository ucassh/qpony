<?php

namespace AppBundle\Service;


use AppBundle\Calculation\Core\Calculator;
use AppBundle\Calculation\Mapper\InputDataToValueObjects;
use AppBundle\Calculation\ValueObject\ValueObject;

class Calculation
{
    /** @var Calculator */
    private $calc;

    public function __construct()
    {
        $this->calc = new Calculator();
    }

    public function getResult($input)
    {
        $mapper = new InputDataToValueObjects($input);
        $calc = $this->calc;
        return array_map(
            function (ValueObject $item) use ($calc) {
                return  [$item->value() => $calc->getResultFor($item->value())];
            },
            $mapper->getValueObjects()->getArrayCopy()
        );
    }
}